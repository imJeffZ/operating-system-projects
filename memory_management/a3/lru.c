#include <stdio.h>
#include <assert.h>
#include <unistd.h>
#include <getopt.h>
#include <stdlib.h>
#include "pagetable.h"


extern int memsize;

extern int debug;

extern struct frame *coremap;

struct node {
	struct node *pre;
	struct node *next;
	int frame;
} typedef node;

// Doubly Ended Queue, implemented using doubly linked list
struct deque {
	node *head;
	node *tail;
} typedef deque;

node **lru_node_ptr = NULL;
deque *lru_queue = NULL;

// Move a linked list node to head
void move_to_head(node *node, deque *queue) {
	struct node *pre = node->pre;
	struct node *next = node->next;
	// If node is already head, do nothing
	if (pre == NULL) {
		return;
	}

	pre->next = next;
	// If node is not a tail
	if (next != NULL) {
		next->pre = pre;
	}
	
	struct node *old_head = queue->head;
	node->pre = NULL;
	node->next = old_head;
	old_head->pre = node;
	
	queue->head = node;
	return;
}

// Move a linked list node to tail
void move_to_tail(node *node, deque *queue) {
	struct node *pre = node->pre;
	struct node *next = node->next;

	// If node is already a tail, do nothing
	if (next == NULL) {
		return;
	}
	next->pre = pre;
	// If node is not a head
	if (pre != NULL) {
		pre->next = next;
	}

	struct node *old_tail = queue->tail;
	node->next = NULL;
	node->pre = old_tail;
	old_tail->next = node;

	queue->tail = node;
	return;
}

/* Page to evict is chosen using the accurate LRU algorithm.
 * Returns the page frame number (which is also the index in the coremap)
 * for the page that is to be evicted.
 */
int lru_evict() {
	struct node *tail = lru_queue->tail;
	move_to_head(tail, lru_queue);
	return tail->frame;
}

/* This function is called on each access to a page to update any information
 * needed by the lru algorithm.
 * Input: The page table entry for the page that is being accessed.
 */
void lru_ref(pgtbl_entry_t *p) {
	struct node *node = lru_node_ptr[((p->frame)% (memsize - 1))];
	move_to_head(node, lru_queue);
	return;
}


/* Initialize any data structures needed for this 
 * replacement algorithm 
 */
void lru_init() {
	// Set up global variables
	lru_node_ptr = malloc(memsize * sizeof(node *));
	lru_queue = malloc(sizeof(deque));

	// Set up all the linked list
	struct node *pre_node = NULL;
	struct node *curr_node = NULL;
	for (int i = 0; i < memsize; i++) {
		curr_node = malloc(sizeof(node));
		lru_node_ptr[i] = curr_node;
		curr_node->frame = i;
		curr_node->pre = pre_node;
		pre_node = curr_node;
	}
	for (int i = 0; i < memsize - 1; i++) {
		lru_node_ptr[i]->next = lru_node_ptr[i+1];
	}
	if (memsize > 0) {
		lru_node_ptr[memsize-1]->next = NULL;
		lru_queue->head = lru_node_ptr[0];
		lru_queue->tail = lru_node_ptr[memsize - 1];
	} else {
		lru_queue->head = NULL;
		lru_queue->tail = NULL;
	}

}
