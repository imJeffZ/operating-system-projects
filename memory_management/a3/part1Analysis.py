import sys

## This is part 1 analysis program.
## To analysis the memory reference, run "python part1Analysis.py xxxxx.ref", 
## where xxxxx.ref is the memory reference file in terminal.
## It would produce an analysis table called "roughAnalysis.txt" for the memory
## reference file.
## We have also uploaded analysis.txt containing info about 3 provided traces.

if __name__== "__main__":
    file_name = sys.argv[1]
    f = open(file_name, "r")
    line = f.readline()
    counts = {"Instructions": 0, "Loads": 0, "Stores": 0, "Modifies": 0}
    instructions = {}
    data = {}
    while line:
        if len(line.split()) < 2:
            break
        op = line.split()[0]
        add1 = line.split()[1].split(",")[0]
        s = list(add1)
        s[-1] = '0'
        s[-2] = '0'
        s[-3] = '0'
        add = ''.join(s)
        if op == "I":
            counts["Instructions"] = counts["Instructions"] + 1
            if not add in instructions:
                instructions[add] = 0
            if add in instructions:
                instructions[add] = instructions[add] + 1
        else:
            if not add in data:
                data[add] = 0
            if add in data:
                data[add] = data[add] + 1
            if op == "L":
                counts["Loads"] = counts["Loads"] + 1
            if op == "M":
                counts["Modifies"] = counts["Modifies"] + 1
            if op == "S":
                counts["Stores"] = counts["Stores"] + 1
        line = f.readline()
    f.close()
    print(counts)
    print(instructions)
    print(data)
    f = open("roughAnalysis.txt","w+")
    f.write("Counts:\r\n")
    f.write("  Instructions %d\r\n" %counts["Instructions"])
    f.write("  Loads        %d\r\n" %counts["Loads"])
    f.write("  Stores       %d\r\n" %counts["Stores"])
    f.write("  Modifies     %d\r\n" %counts["Modifies"])
    f.write("Instructions:\r\n")
    for i in instructions:
         f.write(i + ", " + str(instructions[i]) + "\r\n")
    f.write("Data:\r\n")
    for i in data:
        f.write(i + ", " + str(data[i]) + "\r\n")    
    f.close()