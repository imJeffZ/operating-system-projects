/*
 * This code is provided solely for the personal and private use of students
 * taking the CSC369H course at the University of Toronto. Copying for purposes
 * other than this use is expressly prohibited. All forms of distribution of
 * this code, including but not limited to public repositories on GitHub,
 * GitLab, Bitbucket, or any other online platform, whether as given or with
 * any changes, are expressly prohibited.
 *
 * Authors: Alexey Khrabrov, Andrew Pelegris, Karen Reid
 *
 * All of the files in this directory and all subdirectories are:
 * Copyright (c) 2019 Karen Reid
 */

/**
 * CSC369 Assignment 2 - Message queue implementation.
 *
 * You may not use the pthread library directly. Instead you must use the
 * functions and types available in sync.h.
 */

#include <assert.h>
#include <errno.h>
#include <stdlib.h>

#include "errors.h"
#include "list.h"
#include "msg_queue.h"
#include "ring_buffer.h"


// Message queue implementation backend
typedef struct mq_backend {
	// Ring buffer for storing the messages
	ring_buffer buffer;

	// Reference count
	size_t refs;

	// Number of handles open for reads
	size_t readers;
	// Number of handles open for writes
	size_t writers;

	// Set to true when all the reader handles have been closed. Starts false
	// when they haven't been opened yet.
	bool no_readers;
	// Set to true when all the writer handles have been closed. Starts false
	// when they haven't been opened yet.
	bool no_writers;

	//TODO - Add the necessary synchronization variables.
	mutex_t lock;
	cond_t empty;
	cond_t fill;

} mq_backend;


static int mq_init(mq_backend *mq, size_t capacity)
{
	if (ring_buffer_init(&mq->buffer, capacity) < 0) return -1;

	mq->refs = 0;

	mq->readers = 0;
	mq->writers = 0;

	mq->no_readers = false;
	mq->no_writers = false;

	//TODO
	mutex_init(&(mq->lock));
	cond_init(&(mq->empty));
	cond_init(&(mq->fill));

	return 0;
}

static void mq_destroy(mq_backend *mq)
{
	assert(mq->refs == 0);
	assert(mq->readers == 0);
	assert(mq->writers == 0);

	//TODO
	mutex_destroy(&(mq->lock));
	cond_destroy(&(mq->empty));
	cond_destroy(&(mq->fill));
	ring_buffer_destroy(&(mq->buffer));
}


#define ALL_FLAGS (MSG_QUEUE_READER | MSG_QUEUE_WRITER | MSG_QUEUE_NONBLOCK)

// Message queue handle is a combination of the pointer to the queue backend and
// the handle flags. The pointer is always aligned on 8 bytes - its 3 least
// significant bits are always 0. This allows us to store the flags within the
// same word-sized value as the pointer by ORing the pointer with the flag bits.

// Get queue backend pointer from the queue handle
static mq_backend *get_backend(msg_queue_t queue)
{
	mq_backend *mq = (mq_backend*)(queue & ~ALL_FLAGS);
	assert(mq);
	return mq;
}

// Get handle flags from the queue handle
static int get_flags(msg_queue_t queue)
{
	return (int)(queue & ALL_FLAGS);
}

// Create a queue handle for given backend pointer and handle flags
static msg_queue_t make_handle(mq_backend *mq, int flags)
{
	assert(((uintptr_t)mq & ALL_FLAGS) == 0);
	assert((flags & ~ALL_FLAGS) == 0);
	return (uintptr_t)mq | flags;
}


static msg_queue_t mq_open(mq_backend *mq, int flags)
{
	++mq->refs;

	if (flags & MSG_QUEUE_READER) {
		++mq->readers;
		mq->no_readers = false;
	}
	if (flags & MSG_QUEUE_WRITER) {
		++mq->writers;
		mq->no_writers = false;
	}

	return make_handle(mq, flags);
}

// Returns true if this was the last handle
static bool mq_close(mq_backend *mq, int flags)
{
	assert(mq->refs != 0);
	assert(mq->refs >= mq->readers);
	assert(mq->refs >= mq->writers);

	if (flags & MSG_QUEUE_READER) {
		if (--mq->readers == 0) mq->no_readers = true;
	}
	if (flags & MSG_QUEUE_WRITER) {
		if (--mq->writers == 0) mq->no_writers = true;
	}

	if (--mq->refs == 0) {
		assert(mq->readers == 0);
		assert(mq->writers == 0);
		return true;
	}
	return false;
}


msg_queue_t msg_queue_create(size_t capacity, int flags)
{
	if (flags & ~ALL_FLAGS) {
		errno = EINVAL;
		report_error("msg_queue_create");
		return MSG_QUEUE_NULL;
	}

	mq_backend *mq = (mq_backend*)malloc(sizeof(mq_backend));
	if (mq == NULL) {
		report_error("malloc");
		return MSG_QUEUE_NULL;
	}
	// Result of malloc() is always aligned on 8 bytes, allowing us to use the
	// 3 least significant bits of the handle to store the 3 bits of flags
	assert(((uintptr_t)mq & ALL_FLAGS) == 0);

	if (mq_init(mq, capacity) < 0) {
		// Preserve errno value that can be changed by free()
		int e = errno;
		free(mq);
		errno = e;
		return MSG_QUEUE_NULL;
	}

	return mq_open(mq, flags);
}

msg_queue_t msg_queue_open(msg_queue_t queue, int flags)
{
	if (!queue) {
		errno = EBADF;
		report_error("msg_queue_open");
		return MSG_QUEUE_NULL;
	}

	if (flags & ~ALL_FLAGS) {
		errno = EINVAL;
		report_error("msg_queue_open");
		return MSG_QUEUE_NULL;
	}

	mq_backend *mq = get_backend(queue);

	//TODO
	mutex_lock(&(mq->lock));
	msg_queue_t new_handle = mq_open(mq, flags);
	mutex_unlock(&(mq->lock));
	return new_handle;
}

int msg_queue_close(msg_queue_t *queue)
{
	if (!*queue) {
		errno = EBADF;
		report_error("msg_queue_close");
		return -1;
	}

	mq_backend *mq = get_backend(*queue);

	//TODO
	mutex_lock(&(mq->lock));
	if (mq_close(mq, get_flags(*queue))) {
		// Closed last handle; destroy the queue
		mutex_unlock(&(mq->lock));
		mq_destroy(mq);
		free(mq);
		*queue = MSG_QUEUE_NULL;
		return 0;
	}

	//TODO
	if (mq->no_writers){
		cond_broadcast(&(mq->fill));
	}
	if (mq->no_readers){
		cond_broadcast(&(mq->empty));
	}
	mutex_unlock(&(mq->lock));
	*queue = MSG_QUEUE_NULL;
	return 0;
}


ssize_t msg_queue_read(msg_queue_t queue, void *buffer, size_t count)
{
	//TODO
	mq_backend *mq = get_backend(queue);
	//return -1 in non block mode while ring buffer is empty
	if (get_flags(queue) == MSG_QUEUE_NONBLOCK){
		mutex_lock(&(mq->lock));
		if (!ring_buffer_used(&(mq->buffer))){
			errno = EAGAIN;
			report_error("msg_queue_read");
			mutex_unlock(&(mq->lock));
			return -1;
		}
		mutex_unlock(&(mq->lock));
	}
	//return -1 in write mode
	if (get_flags(queue) == MSG_QUEUE_WRITER){
		errno = EBADF;
		report_error("msg_queue_read");
		return -1;
	}
	//read mode, wait untill buffer is not empty
	mutex_lock(&(mq->lock));
	//returned 0 if the queue is empty and the writers are closed
	if (!ring_buffer_used(&(mq->buffer)) && mq->no_writers){
		mutex_unlock(&(mq->lock));
		return 0;
	}
	while(!ring_buffer_used(&(mq->buffer)) && !(mq->no_writers)){
		cond_broadcast(&(mq->empty));
		cond_wait(&(mq->fill), &(mq->lock));
	}
	//returned 0 if the queue is empty and the writers are closed
	if (!ring_buffer_used(&(mq->buffer)) && mq->no_writers){
		mutex_unlock(&(mq->lock));
		return 0;
	}
	size_t size;
	//check if the buffer is large enough to hold the message
	ring_buffer_peek(&(mq->buffer), &size, sizeof(size_t));
	if (size <= count){
		ring_buffer_read(&(mq->buffer), &size, sizeof(size_t));
		ring_buffer_read(&(mq->buffer), buffer, size);
		cond_broadcast(&(mq->empty));
		mutex_unlock(&(mq->lock));
		return size;
	}
	else{
		errno = EMSGSIZE;
		report_error("msg_queue_read");
		cond_signal(&(mq->fill));
		mutex_unlock(&(mq->lock));
		return -size;
	}
}

int msg_queue_write(msg_queue_t queue, const void *buffer, size_t count)
{
	//TODO
	mq_backend *mq = get_backend(queue);
	//return -1 in non block mode and ring buffer has no enough space to hold message
	if (get_flags(queue) == MSG_QUEUE_NONBLOCK){
		mutex_lock(&(mq->lock));
		if (ring_buffer_free(&(mq->buffer)) < (sizeof(size_t) + count)){
			errno = EAGAIN;
			report_error("msg_queue_write");
			mutex_unlock(&(mq->lock));
			return -1;
		}
		mutex_unlock(&(mq->lock));
	}
	//return -1 in read mode
	if (get_flags(queue) == MSG_QUEUE_READER){
		errno = EBADF;
		report_error("msg_queue_write");
		return -1;
	}
	//return -1 if count = 0
	if (!count){
		errno = EINVAL;
		report_error("msg_queue_write");
		return -1;
	}
	//return -1 if queue buffer is not large enough to hold the message
	if ((mq->buffer).size < (sizeof(size_t) + count)){
		errno = EMSGSIZE;
		report_error("msg_queue_write");
		return -1;
	}
	//return -1 if all reader handles to the queue have been closed
	mutex_lock(&(mq->lock));
	if (mq->no_readers){
		errno = EPIPE;
		report_error("msg_queue_write");
		mutex_unlock(&(mq->lock));
		return -1;
	}
	//wait untill there are free space
	while ((ring_buffer_free(&(mq->buffer)) < (sizeof(size_t) + count)) && !(mq->no_readers)) {
		cond_broadcast(&(mq->fill));
		cond_wait(&(mq->empty), &(mq->lock));
	}
	//return -1 if there are no free space and there are no readers
	if ((ring_buffer_free(&(mq->buffer)) < (sizeof(size_t) + count)) && mq->no_readers){
		errno = EPIPE;
		report_error("msg_queue_write");
		mutex_unlock(&(mq->lock));
		return -1;
	}
	//write if there are free space
	ring_buffer_write(&(mq->buffer), &count, sizeof(size_t));
	ring_buffer_write(&(mq->buffer), buffer, count);
	cond_broadcast(&(mq->fill));
	mutex_unlock(&(mq->lock));
	return 0;
}


int msg_queue_poll(msg_queue_pollfd *fds, size_t nfds)
{
	//TODO
	if (!nfds) {
		errno = EINVAL;
		report_error("msg_queue_poll");
		return -1;
	}
	for (size_t i = 0; i < nfds; i++){
		if (fds[i].queue != MSG_QUEUE_NULL){
			//event flags are invalid
			if(!(fds[i].events & (MQPOLL_READABLE | MQPOLL_WRITABLE | MQPOLL_NOREADERS | MQPOLL_NOWRITERS))){
				errno = EINVAL;
				report_error("msg_queue_poll");
				return -1;
			}
			//no events
			if (!(fds[i].events)){
				errno = EINVAL;
				report_error("msg_queue_poll");
				return -1;
			}
			//MQPOLL_READABLE requested for a non-reader queue handle 
			if (!(fds[i].queue & MSG_QUEUE_READER) && (fds[i].events & MQPOLL_READABLE)){
				errno = EINVAL;
				report_error("msg_queue_poll");
				return -1;
			}
			//MQPOLL_WRITABLE requested for a non-writer queue handle
			if (!(fds[i].queue & MSG_QUEUE_WRITER) && (fds[i].events & MQPOLL_WRITABLE)){
				errno = EINVAL;
				report_error("msg_queue_poll");
				return -1;
			}
		}
	}
	return -1;
}
